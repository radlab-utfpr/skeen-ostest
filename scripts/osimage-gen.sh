#!/usr/bin/env bash

# This script depends on the distro having mkosi installed
#
# Bruno Meneguele - 2021

function _help_and_exit
{
	echo "This script creates the operating system image to be used with"
	echo "skeen-linux testing environment."
	echo
	echo "Usage: osimage-gen.sh [OPTIONS] <mkosi-file> <osimage-path>"
	echo "Options:"
	echo "    -f --force           overwrite the an already existent os image"
	echo "    -k --kernel <name>   kernel name used to get the content of /lib/modules"
	echo "    -h --help            this help message"
	exit $1
}

which mkosi >/dev/null
if [ $? -ne 0 ]; then
	echo "mkosi executable not found"
	exit 1
fi

which tar >/dev/null
if [ $? -ne 0 ]; then
	echo "mkosi executable not found"
	exit 1
fi

while [ ! -z $1 ]; do
	case $1 in
		-f|--force)
			force="--force"
			;;
		-k|--kernel)
			kernel="$2"
			shift
			;;
		-h|--help)
			_help_and_exit 0
			;;
		*)
			if [[ $1 =~ ^-.* ]]; then
				_help_and_exit 1
			else
				break
			fi
			;;
	esac

	shift
done

[ -n "$1" ] && mkosi_file=$1 || _help_and_exit 1
[ -n "$2" ] && osimage_path=$2 || _help_and_exit 1

mkosi_file_path=$(dirname $mkosi_file)
mkosi_extra_tree=$mkosi_file_path/mkosi.extra

# clean and recreated necessary artifacts
rm -f ${mkosi_extra_tree}.tar
[ -e $mkosi_extra_tree ] && unlink $mkosi_extra_tree
ln -s /lib/modules/$kernel $mkosi_extra_tree

sudo mkosi $force -C $mkosi_file_path --default $(basename $mkosi_file) -O $PWD/$osimage_path