#!/usr/bin/env bash

# This script depends on the distro having dracut installed
#
# Bruno Meneguele - 2020

function _help_and_exit
{
	echo "This script creates the initramfs to be used with skeen-linux testing environment."
	echo "It considers by default that skeen module is compiled as builtin."
	echo "Remember you have ran kernel-compile.sh before."
	echo
	echo "Usage: initrd-gen.sh [OPTIONS] <ramdisk-path> <kernel-name>"
	echo "Options:"
	echo "    -m --module    generates ramdisk considering skeen as a loadable module instead of builtin"
	echo "    -h --help      this help message"
	exit $1
}

which dracut >/dev/null
if [ $? -ne 0 ]; then
	echo "dracut executable not found"
	exit 1
fi

skeen_mod=0

while [ ! -z $1 ]; do
	case $1 in
		-m|--module)
			skeen_mod=1
			;;
		-h|--help)
			_help_and_exit 0
			;;
		*)
			if [[ $1 =~ ^-.* ]]; then
				_help_and_exit 1
			else
				break
			fi
			;;
	esac

	shift
done

[ -n "$1" ] && rd_path=$1 || _help_and_exit 1
[ -n "$2" ] && kver=$2 || _help_and_exit 1
[ $skeen_mod -eq 1 ] && drivers="--add-drivers=\"skeen crypto_aes\""

sudo dracut -v -f $drivers $rd_path $kver
sudo chown $(id -u):$(id -g) $rd_path
