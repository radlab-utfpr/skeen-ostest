#!/usr/bin/env bash

# This script make use of 'dnf' for installing some system dependencies. 
# If you're not using Fedora, please, change the respective lines to the correct ones on your distro.
# It basically creates a default testing environment for skeen-linux.
#
# Bruno Meneguele - 2020

function _help_and_exit
{
	echo "This script automates the kernel compilation step."
	echo "It considers by default that skeen module is compiled as builtin."
	echo
	echo "Usage: kernel-compile.sh [OPTIONS]"
	echo "Options:"
	echo "    -b --base      build only the base kernel, SKEEN is not set"
	echo "    -m --module    set skeen module options as a loadable module"
	echo "    -u --update    only update the skeen module"
	echo "    -h --help      this help message"
	exit $1
}

distro="$(sed -n 's/^NAME=\(.*\)/\1/p' /etc/os-release)"
if [[ "$distro" =~ (F|f)edora ]]; then
	pkg_inst=""
	rpm -qi glibc-static >/dev/null
	[ $? -ne 0 ] && pkg_inst+="glibc-static "

	rpm -qi elfutils >/dev/null
	[ $? -ne 0 ] && pkg_inst+="elfutils "

	[ -z "$pkg_inst" ] || sudo dnf -y install $pkg_inst
else
	echo
	echo "please, make sure you have (names are distro dependent):"
	echo "	glibc static library"
	echo "	flex, bison, yacc and bc"
	echo "	libelf-dev, libelf-devel or elfutils-libelf-devel"
	echo "	libssl-dev or openssl-devel"
	echo "installed before compiling."
	echo
fi

skeen_opt="-e"
update_only=0

while [ -n "$1" ]; do
	case $1 in
		-b|--base)
			skeen_opt=""
			;;
		-u|--update)
			update_only=1
			;;
		-h|--help)
			_help_and_exit 0
			;;
		*)
			_help_and_exit 1
			;;
	esac

	shift
done

export KCFLAGS=-ggdb3
if [ $update_only -eq 0 ]; then
	echo "----------------"
	grep skeen Makefile >/dev/null
	[ $? -ne 0 ] && sed -i 's/\(EXTRAVERSION =.*\)/\1-skeen/' Makefile

	make mrproper
	make x86_64_defconfig
	make kvm_guest.config	
	# disable useless subsystems
	./scripts/config -d DRM -d AGP
	./scripts/config -d BPF -d NETFILTER
	./scripts/config -d SOUND -d SND
	./scripts/config -d HID -d HIDRAW -d USB -d INPUT
	# enable debug information
	./scripts/config -e DEBUG_INFO -e DEBUG_KERNEL -e DEBUG_INFO_DWARF4
	./scripts/config -e DEBUG_SECTION_MISMATCH -e DEBUG_OBJECTS -e DEBUG_OBJECTS_WORK -e DEBUG_VM
	./scripts/config -e GDB_SCRIPTS -e HEADERS_INSTALL -e FRAME_POINTER
	# disable kaslr
	./scripts/config -d RANDOMIZE_BASE
	# enable runtime tracing and debugging
	./scripts/config -e TRACING_SUPPORT
	./scripts/config -e PERF_EVENTS -e UPROBE_EVENTS -e KPROVE_EVENTS
	./scripts/config -e FTRACE -e DYNAMIC_FTRACE -e FTRACE_SYSCALLS -e FPROBE
	./scripts/config -e FUNCTION_PROFILER -e FUNCTION_TRACER -e FUNCTION_GRAPH_TRACER
	./scripts/config -e STRACK_TRACE -e BOOTTIME_TRACING
	if [ "x$skeen_opt" != "x" ]; then
		./scripts/config -e USERMODE_DRIVER
		./scripts/config $skeen_opt SKEEN $skeen_opt SKEEN_CRYPTO $skeen_opt SKEEN_CRYPTO_AES
		./scripts/config -e SKEEN_SGX_DEBUG -e SKEEN_SGX_MODE_SIM
		./scripts/config --set-str SKEEN_SGX_SDK_PATH "$HOME/bin/sgx/sgxsdk"
		./scripts/config --set-str SKEEN_SGX_SIGN_KEY "$HOME/git/skeen-linux/security/skeen/sgx_sign_key.pem"
	fi
fi

make -j$(nproc)
make -j$(nproc) modules
sudo make -j$(nproc) modules_install
