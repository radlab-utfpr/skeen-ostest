# skeen-ostest
SKEEN testing scripts

## Installation

Before you run the script, make sure you have all Perl dependencies installed
(listed below). At the same time, feel free to install them from your distro
repository or from CPAN. In here I'm going to show the installation command
using `cpanminus` command, which can be installed either directly from CPAN or
your distro's repo.

```shell
$ cpanm File::Which
```

## Get GDB running

An example on how to start GDB using a kernel image built with the script
placed in `scripts/kernel-compile.sh` and which is running over an QEMU
instance started by `run.pl`:

```
$ echo "add-auto-load-safe-path <kernel-src>/vmlinux-gdb.py" >> ~/.gdbinit
$ gdb <kernel-src>/vmlinux
(gdb) target remote <ip>:<port>
(gdb) hbreak [<func-name> | <file:line>]
(gdb) continue
...
(gdb) step
...
```
