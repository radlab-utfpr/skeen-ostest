#!/usr/bin/env perl

# Script for running the test environment for skeen-linux project
#
# Bruno Meneguele - 2020

use warnings;
use strict;
use utf8;
use v5.26;

use feature qw(signatures);
no warnings qw(experimental::signatures);

use Carp;
use Getopt::Std;
use File::Which;

$Getopt::Std::STANDARD_HELP_VERSION = 1;
$Getopt::Std::OUTPUT_HELP_VERSION = 0;
our $VERSION = "0.1";

sub qemu_check_paths($paths)
{
	while (my ($opt, $path) = each %$paths) {
		if ($opt eq "exec") {
			if (not defined which($path)) {
				croak "$path executable not found, please install it\n";
			}
		} else {
			if (not -e $path) {
				croak "$path file not found\n";
			}
		}
	}
}

sub qemu_call($opts)
{
	my %paths = %{$opts->{paths}};

	qemu_check_paths(\%paths);

	my $exec = $paths{exec};
	my $kernel = $paths{kernel};
	my $initrd = $paths{initrd};
	my $append = $opts->{append};
	my $extra = $opts->{extra};
	my $drive = '';

	if ($paths{image}) {
		$drive = "-drive format=$opts->{format},file=$paths{image}";
	}

	system("$exec " .
		"-kernel $kernel " .
		"-initrd $initrd " .
		"-append \"$append\" " .
		"$drive " .
		"-nographic -enable-kvm -cpu host -m 512 $extra");
}

my %args;
getopts("lk:r:a:e:i:f:", \%args) or die;

my %default = (
	paths => {
		exec => 'qemu-system-x86_64',
		kernel => '../skeen-linux/arch/x86_64/boot/bzImage',
		initrd => 'ramdisk/ramdisk.img',
	},
	format => 'raw',
	# KASLR should be already disabled from defconfig, but lets make sure we
	# don't run a kernel with it enabled.
	append => 'console=ttyS0 nokaslr',
);

my %qemu_opts = (
	paths => {
		exec => $default{paths}->{exec},
		kernel => $args{k} // $default{paths}->{kernel},
		initrd => $args{r} // $default{paths}->{initrd},
	},
	append => $args{a} // $default{append},
	extra => $args{e} // '',
);

if ($args{i} && $args{i} ne '') {
	$qemu_opts{paths}->{image} = $args{i};
	$qemu_opts{format} = $args{f} // $default{format};
	$qemu_opts{append} .= ' root=/dev/sda1 rw rootfstype=ext4';
}

if (not $args{l}) {
	$qemu_opts{extra} .= '-s -S';
	say "[ press 'ctrl+a x' to exit ]";
}

sub HELP_MESSAGE
{
	print <<~ '_END_HELP';
	Usage: ./run.pl [OPTIONS]

	Run QEMU using the custom kernel, initramfs and OS image for testing the
	SKEEN implementation.

	Options:
		-l          run qemu directly, without waiting gdb remote access
		-k <path>   kernel bzimage path
		-r <path>   initramfs image path
		-i <path>   operating system image
		-f <string> operating system image format
		-a <string> string passed as qemu --append option
		-e <string> extra options passed to qemu during its execution
		--help      this help message
		--version   script version
	_END_HELP
}

sub VERSION_MESSAGE
{
	print <<~ "_END_VERSION";
	skeen-ostest v$VERSION - alpha
	_END_VERSION
}

qemu_call(\%qemu_opts);
